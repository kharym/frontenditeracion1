//Dependencias
import React from 'react';
import {Route,Switch} from 'react-router-dom';

//Componentes
import App from './components/App';
import Home from './components/Home';
import MenuQuiz from './components/MenuQuiz';
import Notas from './components/Notas';
import Historial from './components/Historial';
import LoginAlumno from './components/LoginAlumno';
import Page404 from './components/Page404';
import Quiz1 from './components/MenuQuiz/quiz';

const AppRoutes = () =>
  <App>
    <Switch>
      <Route path = "/home" component = {Home} />
      <Route path = "/menuquiz" component = {MenuQuiz} />
      <Route path = "/quiz1" component = {Quiz1} />
      <Route path = "/notas" component = {Notas} />
      <Route path = "/historial" component = {Historial} />
      <Route path = "/" component = {LoginAlumno} />
      <Route component = {Page404} />
    </Switch>
  </App>;

export default AppRoutes;
