import React, { Component } from 'react';

class Notas extends Component {
  render(){
    return(
      <div className = "Notas">
        <h1>Mis Notas</h1>
        <div class="container">
          <table class="table table-bordered">
            <thead thead class="thead-dark">
              <tr>
                <th>Alumno</th>
                <th>Quiz 1</th>
                <th>Quiz 2</th>
                <th>Quiz 3</th>
                <th>Quiz 4</th>
                <th>Quiz 5</th>
                <th>Quiz 6</th>
                <th>Quiz 7</th>
                <th>Quiz 8</th>
                <th>Quiz 9</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Juan Perez</td>
                <td>3,0</td>
                <td>7,0</td>
                <td>5,0</td>
                <td>1,0</td>
                <td>3,0</td>
                <td>3,0</td>
                <td>5,0</td>
                <td>5,0</td>
                <td>7,0</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Notas;
