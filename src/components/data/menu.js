export default[

  {
    title: 'Home',
    url:'/home'
  },

  {
    title: 'MenuQuiz',
    url:'/menuquiz/quiz'
  },

  {
    title: 'Notas',
    url:'/notas'
  },

  {
    title: 'Historial',
    url:'/historial'
  },

  {
    title: 'Cerrar Sesión',
    url:'/cerrar'
  }

];
