//Dependencias
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Componentes
import Header from './Header';
import Calculadora from './Calculadora';
import Content from './Content';
import Footer from './Footer';

//Data
import items from './data/menu';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const { children } = this.props;
    return (
      <div className="App">
        <Header items={items} title="ads"/>
        <Content body = {children}/>
        <Footer/>
      </div>
    );
  }
}

export default App;
// &copy; es para que ponga el simbolo de la c con circulo
