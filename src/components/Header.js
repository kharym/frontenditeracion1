//Dependencias
import React, { Component } from 'react';
import PropTypes from 'prop-types';                    //validacion que ayuda a definir que propiedades se reciben, de que tipo? y confirmar si es que son necesarias o requeridas o no.
import { Link } from 'react-router-dom';

//Assets
import './css/Header.css';

class Header extends Component {

  static propTypes ={

    title: PropTypes.string.isRequired,             // se establece el tipo de dato y se dice que son requeridos obligadamente
    items: PropTypes.array.isRequired
  };

  render() {
    const {title, items} = this.props;             // se obtiene los valores de los objetos title e items.

    return (
      <div className="Header ">
        <div className="row">
          <div className="col-md-2 mb-2">
            <img src={require('./images/logousach.png')} width="53%" height="72%" alt=""/>
          </div>
          <div className="col ">
            <h1> Fundamentos de Programación </h1>

          <ul className = "Menu">
          {
            items && items.map(
              (item,key) => <li key = {key}> <Link to={item.url}> {item.title}</Link></li>)}

          </ul>
            </div>
        </div>
      </div>
    );
  }
}

export default Header;
/*pregunta si items esta inicializado. se le asigna un key  a los elementos creados con el map, guarfa la posicion de cada elemento en el array ---> 27*/
