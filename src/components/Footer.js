//Dependencias
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Assets
import './css/Footer.css';

//Componentes


class Footer extends Component {

  static propTypes ={

    copyright: PropTypes.string
  };

  render() {
    const {copyright = '&copy; USACH TINGESO 2018'} = this.props;
    return (
      <div className="Footer">
        <p className="text-center" dangerouslySetInnerHTML={{__html:copyright}}/>
      </div>
    );
  }
}

export default Footer;
//ngerouslySetInnerHTML={{__html:copyright} hace la C con circulo.
