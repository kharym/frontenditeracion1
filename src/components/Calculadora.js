//Dependencias
import React, { Component } from 'react';
import './css/Calculadora.css';

//Componentes

class Calculadora extends Component {
  constructor(){
    super();

    this.state = {
      count:0,
      number1: 0,
      number2: 0,
      resultado: 0
    };

    this.handleCountClick = this.handleCountClick.bind(this);
    this.handleResultadoClick = this.handleResultadoClick.bind(this);
    this.handleInputChanged = this.handleInputChanged.bind(this);
  }

  componentDidMount(){
    this.setState({
      count:1

    });
  }

  handleCountClick(e){
    if(e.target.id === 'add'){
      this.setState({
        count:this.state.count + 1
      });
    }else if(e.target.id === 'sub' && this.state.count > 0){
      this.setState({
        count: this.state.count - 1
      });
    }else{
      this.setState({
        count:0
      });
    }
  }

  handleResultadoClick(e){
    this.setState({
      resultado: this.state.number1 + this.state.number2
    });
  }

  handleInputChanged(e){
    if(e.target.id === "number1"){
      this.setState({
        number1: Number(e.target.value)
      });
    }else{
      this.setState({
        number2: Number(e.target.value)
      });
    }
  }

  render() {
    return (
      <div className="Calculadora">
        <h1>Counter: {this.state.count}</h1>

        <p>
          <button id = "add" onClick = {this.handleCountClick}>+</button>
          <button id = "sub" onClick = {this.handleCountClick}>-</button>
          <button id = "reset" onClick = {this.handleCountClick}>Reset</button>
        </p>
        <h2>Calculadora</h2>

        <p>
          <input id="number1" className = "form-control"  type= "number" value = {this.state.number1} onChange={this.handleInputChanged} />
          +
          <input id="number2" className = "form-control" type= "number" value = {this.state.number2} onChange={this.handleInputChanged}/>

          <button id="resultado" className = "btn" onClick = {this.handleResultadoClick}>=</button>

          {this.state.resultado}
        </p>
      </div>
    );
  }
}

export default Calculadora;
