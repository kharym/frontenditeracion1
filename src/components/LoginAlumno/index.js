import React, { Component } from 'react';

class LoginAlumno extends Component {


  render(){
    return (

      <div className = "container">
        <form className = "card-body" style={{background: "#F8E7D0"}}>
          <div className = "form-group mt-1">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input
              type = "text"
              name = "correo_alumno"
              className = "form-control"
              placeholder = "Correo de alumno"
              onChange = {this.handleInput}
            />
          </div>
          <div className = "form-group mt-4">
            <input
              type = "password"
              name = "pass_alumno"
              className = "form-control"
              placeholder = "password alumno"
              onChange = {this.handleInput}
            />
          </div>
          <button  className="btn btn-primary"  type="submit" style={{background: "#f68d06"}}>
            Ingresar
          </button>
        </form>

      </div>

    );
  }
}

export default LoginAlumno;
