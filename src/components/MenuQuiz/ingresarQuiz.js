import React, { Component } from 'react';
import { render } from 'react-dom';
import brace from 'brace';
import AceEditor from 'react-ace';
import 'brace/mode/java';
import 'brace/theme/github';

function onChange(newValue) {
  console.log('change',newValue);
}



class Notas extends Component {
  render(){
    return(


      <div className = "Notas">
        <h1> QUIZ 1 </h1>
        <AceEditor
  mode="java"
  theme="github"
  onChange={onChange}
  name="UNIQUE_ID_OF_DIV"
  editorProps={{$blockScrolling: true}}
/>,
document.getElementById('example')
      </div>
    );
  }
}

export default Notas;
