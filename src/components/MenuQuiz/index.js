//Dependencias
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class MenuQuiz extends Component {
  render(){
    return(
      <div className = "MenuQuiz">
        <div className = "container">
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th>Quiz N°</th>
                <th>Rendir</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="col">1</th>
                <td>
                <Link to="/quiz1">
                   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button>
                </Link>
                </td>
              </tr>
              <tr>
                <th scope="col">2</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
                </tr>
              <tr>
                <th scope="col">3</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">4</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">5</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">6</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">7</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">8</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
              <tr>
                <th scope="col">9</th>
                <td>   <button id="resultado" className = "btn-primary" style={{background: "#fe9a2e"}}>Rendir Control</button> </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default MenuQuiz;
