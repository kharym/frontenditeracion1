import React, { Component } from 'react';
import { render } from 'react-dom';

class Quiz extends Component {

  constructor(){
    super();

    this.state = {
      respuesta1: "",
      respuesta2: "",
      respuesta3:  "",
      respCorrecta1: "",
      respCorrecta2: "",
      respCorrecta3: "",
      estadoRespuesta1: "",
      estadoRespuesta2: "",
      estadoRespuesta3: "",
      puntaje1: 0,
      puntaje2: 0,
      puntaje3: 0,
      suma:1
    };

    this.handleResultadoClick1 = this.handleResultadoClick1.bind(this);
    this.handleResultadoClick2 = this.handleResultadoClick2.bind(this);
    this.handleResultadoClick3 = this.handleResultadoClick3.bind(this);
    this.handleInputChanged = this.handleInputChanged.bind(this);
    this.handlePuntajeTotalClick = this.handlePuntajeTotalClick.bind(this);
  }

  componentDidMount(){
    this.setState({
      respCorrecta1: this.state.respCorrecta1,
      estadoRespuesta1: this.state.estadoRespuesta1,
      respCorrecta2: this.state.respCorrecta2,
      estadoRespuesta2: this.state.estadoRespuesta2,
      respCorrecta3: this.state.respCorrecta3,
      estadoRespuesta3: this.state.estadoRespuesta3,
      puntaje1: this.state.puntaje1,
      puntaje2: this.state.puntaje2,
      puntaje3: this.state.puntaje3,
      suma: this.state.suma,
    });
  }


  handleInputChanged(e){
    //console.log( e.target.value);
    if(e.target.name === "respuesta_1"){
      console.log("respuesta1");
      this.setState({
        respuesta1: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta_2"){
          console.log("respuesta2");
      this.setState({
        respuesta2: e.target.value.toString()
      });
    }else if (e.target.name === "respuesta_3"){
      this.setState({
        respuesta3: e.target.value.toString()
      });
    }
  }

  handleResultadoClick1(e){
    this.state.respCorrecta1 ="10";
    console.log("handleResultadoClick");
    if(this.state.respuesta1 === this.state.respCorrecta1){
      this.setState({
        estadoRespuesta1: "¡Correcta!",
        respCorrecta1:"10",
        puntaje1:2,
      });
    }else{
      this.setState({
        estadoRespuesta1: "¡Incorrecta!",
        respCorrecta1:"10"
      });
    }
  }

  handleResultadoClick2(e){
    this.state.respCorrecta2 ="abcde";
    console.log("handleResultadoClick");
    if(this.state.respuesta2 === this.state.respCorrecta2){
      this.setState({
        estadoRespuesta2: "¡Correcta!",
        respCorrecta2:"abcde",
        puntaje2:2,
      });
    }else{
      this.setState({
        estadoRespuesta2: "¡Incorrecta!",
        respCorrecta2:"abcde"
      });
    }
  }

  handleResultadoClick3(e){
    this.state.respCorrecta3 ="[1,5,7,10]";
    console.log("handleResultadoClick");
    if(this.state.respuesta3 === this.state.respCorrecta3){
      this.setState({
        estadoRespuesta3: "¡Correcta!",
        respCorrecta3:"[1,5,7,10]",
        puntaje3:2,
      });
    }else{
      this.setState({
        estadoRespuesta3: "¡Incorrecta!",
        respCorrecta3:"[1,5,7,10]"
      });
    }
  }

  handlePuntajeTotalClick(e){
    this.setState({
      suma: this.state.puntaje1 + this.state.puntaje2 + this.state.puntaje3 + 1
    });
  }


  render(){
    return(

      <div className="container">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th>N°</th>
              <th>Preguntas</th>
              <th>Respuesta</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="col">1</th>
                <td>
                  <div className="card">
                    <p className = "text-center" >¿Cuánto es 5 + 5?</p>
                  </div>
                  <div className = "form-group mt-4">
                    <input
                      type = "respuesta1"
                      name = "respuesta_1"
                      className = "container"
                      placeholder = "respuesta 1"
                      onChange = {this.handleInputChanged}
                    />
                  </div>
                  <button  className="btn btn-primary"  type="submit"  onClick = {this.handleResultadoClick1} style={{background: "#f68d06"}}>
                    Enviar Respuesta
                  </button>
                </td>
                <td>
                  <div className="container">

                    <h6>La respuesta es:</h6>
                    <div className="card">
                      <p className = "text-center" >  {this.state.estadoRespuesta1} </p>
                    </div>
                    <h6>El resultado es:</h6>
                    <div className="card">
                      <p className = "text-center" >  {this.state.respCorrecta1} </p>
                    </div>
                    <h6>Puntaje:</h6>
                    <div className="card">
                      <p className = "text-center" >  {this.state.puntaje1} </p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="col">2</th>
                  <td>
                    <div className="card">
                      <p className = "text-center">¿Cuál es el resultado de concatenar a,b,c,d y e?</p>
                    </div>
                    <div className = "form-group mt-4">
                      <input
                        type = "respuesta2"
                        name = "respuesta_2"
                        className = "container"
                        placeholder = "respuesta 2"
                        onChange = {this.handleInputChanged}
                      />
                    </div>
                    <button  className="btn btn-primary"  type="submit" onClick = {this.handleResultadoClick2}  style={{background: "#f68d06"}}>
                      Enviar Respuesta
                    </button>
                  </td>
                  <td>
                    <div className="container">
                      <h6>La respuesta es:</h6>
                      <div className="card">
                        <p className = "text-center" >  {this.state.estadoRespuesta2} </p>
                      </div>
                      <h6>El resultado es:</h6>
                      <div className="card">
                        <p className = "text-center" >  {this.state.respCorrecta2} </p>
                      </div>
                      <h6>Puntaje:</h6>
                      <div className="card">
                        <p className = "text-center" >  {this.state.puntaje2} </p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="col">3</th>
                    <td>
                      <div className="card">
                        <p className = "text-center">Genere una lista con los elementos 1,5,7 y 10.</p>
                      </div>
                      <div className = "form-group mt-4">
                        <input
                          type = "respuesta3"
                          name = "respuesta_3"
                          className = "container"
                          placeholder = "respuesta 3"
                          onChange = {this.handleInputChanged}
                        />
                      </div>
                      <button  className="btn btn-primary"  type="submit" onClick = {this.handleResultadoClick3} style={{background: "#f68d06"}}>
                        Enviar Respuesta
                      </button>
                    </td>
                    <td>
                      <div className="container">
                        <h6>La respuesta es:</h6>
                        <div className="card">
                          <p className = "text-center" >  {this.state.estadoRespuesta3} </p>
                        </div>
                        <h6>El resultado es:</h6>
                        <div className="card">
                          <p className = "text-center" >  {this.state.respCorrecta3} </p>
                        </div>
                        <h6>Puntaje:</h6>
                        <div className="card">
                          <p className = "text-center" >  {this.state.puntaje3} </p>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="col"></th>
                      <td>
                        <button type="button" class="btn btn-primary"  onClick = {this.handlePuntajeTotalClick} style={{background: "#f68d06"}}>
                          Entregar Quiz
                        </button>
                      </td>
                      <td>
                        <div className="card">
                          <h5 className= "card-header">Nota:</h5>
                          <div className="card-body">
                            {this.state.suma}
                          </div>
                        </div>
                      </td>
                    </tr>
            </tbody>
          </table>
        </div>
      );
    }
  }

export default Quiz;
